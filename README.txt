Openid Connect Auto Login
-------------------------

An extension of module "openid_connect". It provides auto-login functionality
where the user gets redirected to the IDP provider automatically on hitting any
page of the web app. It has a configuration page to exclude some paths for this
auto redirection and has a roles setting.

Required Modules:
-----------------
https://www.drupal.org/project/openid_connect

Configuration:
--------------
Enable it and go to admin > Auto Login exclude path Settings
Url: http://sitename.com/admin/config/services/openid-connect-auto-login
Specify the paths to exclude for the autologin in the field 'Exclude Path'.
