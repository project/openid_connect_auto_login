<?php

/**
 * @file
 * Administrative page code for the openid connect auto login module.
 */

/**
 * Administrative settings.
 */
function _openid_connect_auto_login_settings_form() {
  $form = [];

  $form['traversed_paths'] = [
    '#type' => 'fieldset',
    '#title' => t('Traversed paths'),
    '#collapsible' => TRUE,
  ];

  $form['traversed_paths']['openid_connect_auto_login_excluded_paths'] = [
    '#type' => 'textarea',
    '#title' => t('Excluded paths'),
    '#default_value' => variable_get('openid_connect_auto_login_excluded_paths', '/user/login'),
    '#size' => 20,
    '#description' => t('Admin login to hit url:example.com/user/login or set Specify the paths to exclude for the autologin in the field Excluded paths field example: /user/login, /node/1, etc.'),
  ];

  $form['select_roles'] = [
    '#type' => 'fieldset',
    '#title' => t('select roles'),
    '#collapsible' => TRUE,
  ];

  $all_roles = openid_connect_auto_login_get_all_roles();
  $default_value = array();
  $form['select_roles']['openid_connect_auto_login_user_roles'] = [
    '#type' => 'checkboxes',
    '#title' => t('Choose a role for auto login user register'),
    '#options' => $all_roles,
    '#default_value' => variable_get('openid_connect_auto_login_user_roles', $default_value),
  ];

  return system_settings_form($form);
}
