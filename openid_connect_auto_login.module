<?php

/**
 * @file
 * A pluggable client implementation for the openid connect auto login.
 */

/**
 * Implements hook_help().
 */
function openid_connect_auto_login_help($path, $arg) {
  switch ($path) {
    case 'admin/help#openid_connect_auto_login':
      return check_markup(file_get_contents(dirname(__FILE__) . "/README.txt"));
  }
}

/**
 * Implements hook_menu().
 */
function openid_connect_auto_login_menu() {
  $items = [];
  $items['admin/config/services/openid-connect-auto-login'] = [
    'title' => 'Openid Connect Auto Login Settings',
    'description' => 'Configure Openid Connect Auto Login.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_openid_connect_auto_login_settings_form'),
    'access arguments' => array('administer users'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'openid_connect_auto_login.admin.inc',
  ];
  return $items;
}

/**
 * Implements hook_boot().
 */
function openid_connect_auto_login_boot() {
  if (php_sapi_name() === 'cli') return;

  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  global $user;
  if (!$user->uid) {
    $destination = drupal_get_destination();
    $redirect = !empty($destination['destination']) ? $destination['destination'] : '';
    $client_name = array_shift(variable_get('openid_connect_clients_enabled', array()));
    // Stop multiple redirect and send to destination.
    $multipleredirect = '/openid-connect/' . $client_name . '?destination=' . $redirect;
    $excludedpath = explode(", ", variable_get('openid_connect_auto_login_excluded_paths', '/user/login'));
    $isOpenIdRedirect = stripos($multipleredirect, 'destination=openid-connect');
    if (($_SERVER['REQUEST_URI'] != $multipleredirect && !$isOpenIdRedirect)) {
      // User login as admin or skip some path.
      if (!in_array($_SERVER['REQUEST_URI'], $excludedpath)) {
        openid_connect_save_destination();
        $client = openid_connect_get_client($client_name);
        $scopes = openid_connect_get_scopes();
        $_SESSION['openid_connect_op'] = 'login';
        $client->authorize($scopes);
      }
    }
    else {
      return;
    }
  }
}

/**
 * Perform an action after a successful authorization.
 *
 * @param array $tokens
 *   ID token and access token that we received as a result of the OpenID
 *   Connect flow.
 * @param object $account
 *   The user account that has just been logged in.
 * @param array $userinfo
 *   The user claims returned by the OpenID Connect provider.
 * @param string $client_name
 *   The machine name of the OpenID Connect client plugin.
 * @param bool $is_new
 *   Whether the account has just been created via OpenID Connect.
 */
function openid_connect_auto_login_openid_connect_post_authorize($tokens, $account, $userinfo, $client_name, $is_new) {
  if ($is_new) {
    $uid = $account->uid;
    if (isset($uid)) {
      $userexist = user_load($uid);
      // Add user roles after account has created.
      $userexist->roles = array_filter(variable_get('openid_connect_auto_login_user_roles', array()));
      // Save existing user.
      user_save((object) array('uid' => $userexist->uid), (array) $userexist);
    }
  }
}

/**
 * Fetch all the role information.
 */
function openid_connect_auto_login_get_all_roles($membersonly = TRUE) {
  $roles = user_roles($membersonly = TRUE);
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  return $roles;
}
